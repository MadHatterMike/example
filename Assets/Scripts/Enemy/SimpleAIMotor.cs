﻿using UnityEngine;
using System.Collections;

public class SimpleAIMotor : Motor
{
    Transform _transform;
    CharacterController _controller;
    public float speed = 3.0f;
    public float rotationSpeed = 5.0f;

    public SimpleAIMotor(Transform transfrom, CharacterController controller)
    {
        _transform = transfrom;
        _controller = controller;
    }

    public void setMoveDirection(Vector3 direction)
    {
    }
    public void setRotationDirection(Vector3 direction)
    {
    }
    public void setJump(bool j)
    {
    }
    public void move()
    {
    }

    /*
    void RotateTowards(Vector3 position)
    {
        SendMessage("SetSpeed", 0.0f);

        Vector3 direction = position - transform.position;
        direction.y = 0;
        if (direction.magnitude < 0.1f)
            return;

        // Rotate towards the target
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
    }

    void MoveTowards(Vector3 position)
    {
        var direction = position - transform.position;
        direction.y = 0;
        if (direction.magnitude < 0.5f)
        {
            SendMessage("SetSpeed", 0.0f);
            return;
        }

        // Rotate towards the target
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

        // Modify speed so we slow down when we are not facing the target
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        float speedModifier = Vector3.Dot(forward, direction.normalized);
        speedModifier = Mathf.Clamp01(speedModifier);

        // Move the character
        direction = forward * speed * speedModifier;
        ((CharacterController)GetComponent(typeof(CharacterController))).SimpleMove(direction);

        SendMessage("SetSpeed", speed * speedModifier, SendMessageOptions.DontRequireReceiver);
    }*/
}
