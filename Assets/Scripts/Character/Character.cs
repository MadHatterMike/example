﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
    public Health health;
    public Motor motor;

    private GameObject _gameObject;
    private Transform _transform;

    void Awake()
    {
        _gameObject = gameObject;
        _transform = transform;
    }

}
