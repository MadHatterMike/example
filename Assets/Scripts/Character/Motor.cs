﻿using UnityEngine;
using System.Collections;

public interface Motor
{
    //public virtual Motor( GameObject g, Transform t);
    void setMoveDirection(Vector3 direction);
    void setRotationDirection(Vector3 direction);
    void setJump(bool j);
    void move(); // used in update for now. TODO: Change later

}
