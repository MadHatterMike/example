﻿using UnityEngine;
using System.Collections;

public class CharacterMotor : Motor
{
    private float _speed = 4;
    private Vector3 _direction;
    private Vector3 _velocity;
    private float _gravity = 18;
    private CharacterController _controller;
    private GameObject _gameObject;
    private Transform _transform;
    private bool jump = false;

    public CharacterMotor(GameObject g, Transform t)
    {
        _gameObject = g;
        _transform = t;
        _controller = g.GetComponent<CharacterController>();
    }

    public void setMoveDirection(Vector3 direction)
    {
        if (direction == null)
        {
            _direction = Vector3.zero;
        }
        else
        {
            _direction = direction.normalized;
        }
    }
    public void setRotationDirection(Vector3 direction)
    {
    }

    public void setJump(bool j)
    {
        jump = j;
    }

    public void move()
    {
        Vector3 moveVelocity = _velocity;
        if(_controller.isGrounded)
        {
            moveVelocity = _direction * _speed;

            if (jump)
            {
                moveVelocity.y = 30;
            }
        }
        moveVelocity.y -= _gravity * Time.deltaTime;

        _controller.Move(moveVelocity * Time.deltaTime);
    }

 
}
