﻿using UnityEngine;
using System.Collections;

public class Player : Character
{
    private PlayerInput input;

    void Start()
    {
        input = new PlayerInput();
        health = new Health();
        motor = new CharacterMotor(gameObject, transform);
    }

    void Update()
    {
        motor.setMoveDirection(input.GetMoveDirectionInput());
        motor.setJump(input.GetJump());

        motor.move();
    }

}
