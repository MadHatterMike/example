﻿using UnityEngine;
using System.Collections;

public class PlayerInput 
{
    public Vector3 GetMoveDirectionInput()
    {
        Vector3 dir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        return dir;
    }

    public bool GetJump()
    {
        return Input.GetButtonDown("Jump");
    }

}
