﻿using UnityEngine;
using System.Collections;

public class BasicEnemy : MonoBehaviour
{

    public enum State
    { 
        Idle,
        Attack,
        Investigate,
        Waypoint
    };

    State state = State.Idle;

    // idle variables
    private float minIdleTime = 1;
    private float maxIdleTime = 5;
    public float idleStartTime = 0;
    public float idleLength = 0;

	void Start ()
    {
        StartCoroutine(Idle());
    }

    IEnumerator Idle()
    {
        while (true)
        {
            if (state == State.Idle)
            {
                idleStartTime = Time.time;
                idleLength = Random.Range(minIdleTime, maxIdleTime);
                while (true)
                {
                    Debug.Log("Idle");
                    yield return null;
                    if (Time.time - idleStartTime > idleLength)
                    {
                        yield return StartCoroutine(Waypoint());
                        state = State.Idle;
                        break;
                    }
                }
            }
            yield return null;
        }
    }

    /*
    IEnumerator Attack()
    {
    }


    IEnumerator Investigate()
    {
    }
    */

    IEnumerator Waypoint()
    {
        state = State.Waypoint;
        Debug.Log("Waypoint");
        yield return new WaitForSeconds(5);
    }
}
